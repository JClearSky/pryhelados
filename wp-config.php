<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'pryhelados' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', '127.0.0.1' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '<jyv~#kf.{c xeKwn4t$%%bRxTMgRp]Pc:F2n 7Sss:6&W6M(BuyCP:c:z1gv]~3' );
define( 'SECURE_AUTH_KEY',  '+r(2sA$RbgZwt)SeSSCuAO!+@m`N2eD)xxU_M=8fC]k%Z6I34IWitUKImRy[{5Fd' );
define( 'LOGGED_IN_KEY',    'g,piRL*a342J5I~xS[PhAhH;vgCI>/}VCcB5~bowawVTmDB)qF?|mR>W%M_CLQOp' );
define( 'NONCE_KEY',        'O+@s<a(mXUc{YkdboY~bR0A3sW`%objR_~DwQS[Z@;E/C5{NFXziR(Tvk9RVPDor' );
define( 'AUTH_SALT',        'EPvw7I]vvKc=K:q505;Pb[Ro><5XX-:XB4FO0Rc4r:x.Qvz;M2QsaAAR^UjsL;#g' );
define( 'SECURE_AUTH_SALT', 'e*q]wPapw08(W!BaM^@bjYyTDAAX)XaGU(#*jOTe{K[RHg t5BsB%)b; Ux5OX/n' );
define( 'LOGGED_IN_SALT',   'z/$k9DQ4P!2xk8tr^o#$0ATx;WTnQ(5oVGH?xbM.e%qwDed3H]tcYv(b(Q}O?V24' );
define( 'NONCE_SALT',       'c|TAxY$_IR6[9%zD;p%,P!-/|xHN)w/N&k[nUo_.pWv7V)uUEd1{hQ.u!~b+PH~<' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
